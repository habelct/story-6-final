from django.contrib import admin
from django.urls import path
from . import views

app_name = "homepage"

urlpatterns = [
    path('home/', views.homepage, name='homepage'),
    path('', views.redirecting, name='redirecting'),
]
