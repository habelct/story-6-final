from django.shortcuts import render, redirect
from homepage.models import Status
from homepage.forms import StatusF

# Create your views here.
def homepage(request):
	if request.method == "POST":
		form = StatusF(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/home/')
	else:
		form = StatusF()

	objekStatus = Status.objects.order_by('-date')
	return render(request, 'homepage.html', {'form': form, 'status': objekStatus})

def redirecting(request):
	return redirect('/home/')
