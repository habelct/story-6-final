from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from . import models, forms, views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
from selenium.webdriver import ChromeOptions, Chrome
import unittest
import time

class TestStatusPage(TestCase) :
    def test_Apakah_ada_url(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('/home/')
        self.assertFalse(response.status_code == 404)

    def test_status_post_sukses(self):
        response_post = Client().post('/',{'status':'coba coba'})
        self.assertEqual(response_post.status_code, 302)

    def test_func(self):
        found = resolve('/') 
        self.assertEqual(found.func, views.redirecting)


    def test_apakah_ada_html_status(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'homepage.html')
        
    def test_form_apakah_ada_isi_status(self):
        contoh = 'berhasil'
        form_input = {
            'status' :contoh
        }
        test = forms.StatusF(data = form_input)
        self.assertTrue(test.is_valid())

    def test_model_apakah_isi_dalam_status(self):
        hitung = models.Status.objects.all().count()
        Comment_model = models.Status.objects.create(status="test")
        hitung1 = models.Status.objects.all().count()
        self.assertEqual(hitung, 0)
        self.assertEqual(hitung1, hitung+1)
        self.assertTrue(isinstance(Comment_model, models.Status))

    def test_apakah_ada_button_status(self):
        response = Client().get('/home/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)
        self.assertIn("Status", content)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.opts =ChromeOptions()
        # self.opts.add_argument('dns-prefetch-disable')
        self.opts.add_argument('no-sandbox')
        self.opts.add_argument('headless')  
        self.opts.add_argument('disable-gpu')
        self.browser = Chrome(options=self.opts, executable_path='./chromedriver')
        # self.browser.implicitly_wait(5)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        # self.browser.get('http://127.0.0.1:8000/')
        self.browser.get(self.live_server_url)
        self.assertIn('Status', self.browser.title)

        text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Halo, Apa Kabar?', text)

        btn = self.browser.find_element_by_id('submit-id-submit')
        inputbox1 = self.browser.find_element_by_id('id_status')
        time.sleep(1)
        inputbox1.send_keys('coba coba')
        time.sleep(2)
        btn.click()
        time.sleep(5)

        self.assertIn('coba coba', self.browser.page_source)
