from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from .models import Status

class StatusF(forms.ModelForm):
    status = forms.CharField(max_length=300, widget=forms.Textarea)

    class Meta:
    	model = Status
    	fields = ('status',)
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper
        self.helper.form_method = 'post'

        self.helper.layout = Layout(
            'status',
            Submit('submit', 'Post', css_class = 'btn-dark')
        )


